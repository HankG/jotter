import 'dart:convert';
import 'dart:io';

import 'package:result_monad/result_monad.dart';

import 'models.dart';

class FriendicaClient {
  final String username;
  final String password;
  final String serverName;
  late final String _authHeader;

  FriendicaClient(
      {required this.username,
      required this.password,
      required this.serverName}) {
    final authenticationString = "$username:$password";
    final encodedAuthString = base64Encode(utf8.encode(authenticationString));
    _authHeader = "Basic $encodedAuthString";
  }

  static FriendicaClient fromFile(File file) {
    final json = jsonDecode(file.readAsStringSync());
    final settings = FriendicaSettings.fromMap(json);
    return FriendicaClient(
        username: settings.username,
        password: settings.password,
        serverName: settings.hostname);
  }

  FutureResult<List<dynamic>, String> getTimeline(
      String userId, int page, int count) async {
    final request = Uri.parse(
        'https://$serverName/api/statuses/user_timeline?screen_name=$userId&count=$count&page=$page');
    return (await _getApiRequest(request));
  }

  FutureResult<int, String> postNewStatus(String newStatus) async {
    final request = Uri.parse('https://$serverName/api/v1/statuses');
    final result = await postUrl(request, {"status": newStatus});
    return result.mapValue((value) => value.statusCode);
  }

  FutureResult<HttpClientResponse, String> getUrl(Uri url) async {
    try {
      final client = HttpClient();
      final request = await client.getUrl(url);
      request.headers.add('authorization', _authHeader);
      final response = await request.close();
      client.close(force: true);
      return Result.ok(response);
    } catch (e) {
      return Result.error(e.toString());
    }
  }

  FutureResult<HttpClientResponse, String> postUrl(
      Uri url, Map<String, dynamic> body) async {
    try {
      final client = HttpClient();
      final request = await client.postUrl(url);
      request.headers.add('authorization', _authHeader);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      final jsonBody = jsonEncode(body);
      request.write(jsonBody);
      final response = await request.close();
      client.close(force: true);
      return Result.ok(response);
    } catch (e) {
      return Result.error(e.toString());
    }
  }

  FutureResult<List<dynamic>, String> _getApiRequest(Uri url) async {
    final responseResult = await getUrl(url);
    if (responseResult.isFailure) {
      return responseResult.mapValue((value) => <dynamic>[]);
    }
    final body = await responseResult.value.transform(utf8.decoder).join('');
    final bodyJson = jsonDecode(body) as List<dynamic>;
    return Result.ok(bodyJson);
  }
}
