import 'dart:io';

import 'package:args/args.dart';
import 'package:path/path.dart' as p;

const addToBlogKeyword = "blog";
const addToFriendicaKeyword = "friendica";
const appSettingsFileKeyword = "app-settings";
const friendicaSettingsKeyword = "friendica-settings";
const baseFolderKeyword = "folder";
final defaultBaseFolder = p.join('content', 'jots');

String userHome =
    Platform.environment['HOME'] ?? Platform.environment['USERPROFILE'] ?? '';

ArgParser buildArgs() => ArgParser()
  ..addOption(baseFolderKeyword,
      help: 'Base folder where post hierarchy exists',
      mandatory: false,
      defaultsTo: defaultBaseFolder)
  ..addOption(friendicaSettingsKeyword,
      help: 'Location of Friendica settings file if cross-posting to Friendica',
      mandatory: false,
      defaultsTo: '$userHome/friendica_settings.json')
  ..addOption(appSettingsFileKeyword,
      help: 'Location of Jotter app settings file',
      mandatory: false,
      defaultsTo: '$userHome/jotter_settings.json')
  ..addFlag(addToFriendicaKeyword,
      help: 'Cross-post to friendica account', defaultsTo: false)
  ..addFlag(addToBlogKeyword, help: 'Write to blog jot file', defaultsTo: true);
