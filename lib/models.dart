import 'dart:convert';
import 'dart:io';

class FriendicaSettings {
  final String hostname;
  final String username;
  final String password;

  FriendicaSettings.fromMap(Map<String, dynamic> map)
      : hostname = map['hostname'],
        username = map['username'],
        password = map['password'];
}

class JotterSettings {
  final String blogBaseUrl;

  JotterSettings({required this.blogBaseUrl});

  JotterSettings.fromMap(Map<String, dynamic> map)
      : blogBaseUrl = map['blog-base-url'];

  static JotterSettings fromFile(File file) =>
      JotterSettings.fromMap(jsonDecode(file.readAsStringSync()));
}
