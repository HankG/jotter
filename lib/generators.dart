import 'dart:io';

import 'package:indent/indent.dart';

import 'formats.dart';

String generateHeader(DateTime creationDate) => """
    ---
    title: ${buildTitle(creationDate)}
    url: ${buildUrlLocation(creationDate)}
    date: ${creationDate.toIso8601String()}
    layout: post
    type: jots
    tags : [ "daily jots" ]
    categories : [ "Daily Jots" ]
    include_toc: false
    ---

    """
    .unindent();

String generateEntry(String text, DateTime entryTime) =>
    "**${time24hrFormat.format(entryTime)}:** $text";

void processNewEntry(File file, String entry, DateTime entryTime) {
  late final String baseText;
  if (file.existsSync()) {
    baseText = file.readAsStringSync();
  } else {
    baseText = generateHeader(entryTime);
  }

  final content = '$baseText\n${generateEntry(entry, entryTime)}\n';
  file.writeAsStringSync(content);
}
