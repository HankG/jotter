import 'package:intl/intl.dart';
import 'package:path/path.dart' as p;

const baseName = 'daily-jots';

String buildFileLocation(String baseFolder, DateTime creationTime) => p.join(
    baseFolder, creationTime.year.toString(), buildFileName(creationTime));

String buildUrlLocation(DateTime creationTime) =>
    p.join(p.separator, dayPathFormat.format(creationTime), baseName);

String buildFileName(DateTime creationTime) => '${buildName(creationTime)}.md';

String buildName(DateTime creationTime) =>
    '${dayKabobFormat.format(creationTime)}-$baseName';

String buildTitle(DateTime creationTime) =>
    'Daily Jots for ${longDayFormat.format(creationTime)}';

String buildReferenceText(String refLink) => '(Jotted at: $refLink)';

String buildReferenceUrl(String blogBaseUrl, DateTime creationTime) {
  final baseUrl = Uri.parse(blogBaseUrl);
  final postPath = buildUrlLocation(creationTime);
  final combinedPath = '${baseUrl.origin}$postPath';
  return combinedPath;
}

DateFormat dayKabobFormat = DateFormat('yyyy-MM-dd');
DateFormat dayPathFormat = DateFormat('yyyy/MM/dd');
DateFormat longDayFormat = DateFormat('dd MMM yyyy');
DateFormat time24hrFormat = DateFormat('HH:mm');
