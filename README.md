# Jotter

A personal command line tool application for writing
social media sized posts to my blog in one big entry for
each day.

## Installation

```bash
git clone https://gitlab.com/HankG/jotter.git
cd jotter
dart compile exe -o jotter bin/jotter.dart
```

At this point you can copy the compiled jotter.exe file to some place on your path

## Usage

From the root of your Hugo blog folder execute the `jotter` command.
```bash
jotter [options] <text you want to jot down>
Example: jotter "Checkout (this wiki)[https://en.wikipedia.org/wiki/Firefox]"

Options: 
--folder    Base folder where post hierarchy exists
            (defaults to "content/posts")
```

If the file doesn't exist it writes out the full file with header. 
If the file already exists it appends the status to the file.

### Example
When executing these commands on 23 Jun 2022:

```bash
mkdir /tmp/2022
jotter --folder "/tmp" "This is a first status..."
```

Will produce output to the file `/tmp/2022/2022-06-23-daily-jots.md` that reads:

```plaintext
---
title: Daily Jots for 23 Jun 2022
url: /2022/06/23/daily-jots
date: 2022-06-23T12:15:04.419596
layout: post
type: posts
tags : [ "daily jots" ]
categories : [ "Daily Jots" ]
include_toc: false
---

My jotted thoughts for 23 Jun 2022...

<!--more-->


**12:15:** This is a first status...
```

When I ran it again to add a second status with this command:

```bash
jotter --folder "/tmp" "Second Status"
```

The file now reads:

```plaintext
---
title: Daily Jots for 23 Jun 2022
url: /2022/06/23/daily-jots
date: 2022-06-23T12:15:04.419596
layout: post
type: posts
tags : [ "daily jots" ]
categories : [ "Daily Jots" ]
include_toc: false
---

My jotted thoughts for 23 Jun 2022...

<!--more-->


**12:15:** This is a first status...

**12:17:** Second Status
```

## Limitations

I wrote this tool specifically for my blog. That
means it is very opinionated about folder structure,
date formats, file naming et cetera. 

## License

This is licensed under the Mozilla Public License.