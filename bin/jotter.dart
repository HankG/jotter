import 'dart:io';

import 'package:args/args.dart';
import 'package:jotter/args.dart';
import 'package:jotter/formats.dart';
import 'package:jotter/friendica_client.dart';
import 'package:jotter/generators.dart';
import 'package:jotter/models.dart';

void main(List<String> arguments) async {
  final entryTime = DateTime.now();
  final argParser = buildArgs();
  if (arguments.isEmpty) {
    print('jotter [options] <text you want to jot down>');
    print(
        'Example: jotter "Checkout (this wiki)[https://en.wikipedia.org/wiki/Firefox]"');
    print('');
    print('Options: ');
    print(argParser.usage);
    return;
  }

  late final ArgResults settings;
  try {
    settings = argParser.parse(arguments);
  } on ArgParserException catch (e) {
    print("Error with arguments: ${e.message}");
    print(argParser.usage);
    return;
  }

  final path = buildFileLocation(settings[baseFolderKeyword], DateTime.now());
  final entry = settings.rest.join(' ');

  if (settings[addToFriendicaKeyword]) {
    print("Write to Friendica");
    final friendicaSettingsFile = File(settings[friendicaSettingsKeyword]);
    if (!friendicaSettingsFile.existsSync()) {
      print("Please specify a valid Friendica settings file location");
    }

    final appSettingsFile = File(settings[appSettingsFileKeyword]);
    if (!appSettingsFile.existsSync()) {
      print("Please specify a valid Jotter app settings file location.");
    }
    final appSettings = JotterSettings.fromFile(appSettingsFile);
    final client = FriendicaClient.fromFile(friendicaSettingsFile);
    final referenceText = buildReferenceText(
        buildReferenceUrl(appSettings.blogBaseUrl, entryTime));
    final status = '$entry $referenceText';
    await client.postNewStatus(status);
  }

  if (settings[addToBlogKeyword]) {
    print("Writing to blog");
    processNewEntry(File(path), entry, entryTime);
    print('Wrote update to: $path');
    print(File(path).readAsStringSync());
  }
}
