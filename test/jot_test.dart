import 'package:jotter/generators.dart';
import 'package:test/test.dart';

void main() {
  final entryTime = DateTime.now();
  group('Jot Generator', () {
    test('Header', () {
      print(generateHeader(entryTime));
    });

    test('Entry', () {
      print(generateEntry("This is a new entry...", entryTime));
    });
  });
}
