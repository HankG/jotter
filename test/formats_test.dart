import 'package:jotter/formats.dart';
import 'package:test/test.dart';

void main() {
  final creationTime = DateTime(2021, 7, 8, 10, 30, 55);

  group('Test DateTime String Creation', () {
    test('fileLocation', () {
      expect(buildFileLocation('testPath', creationTime),
          equals('testPath/2021/2021-07-08-daily-jots.md'));
    });

    test('urlLocation', () {
      expect(buildUrlLocation(creationTime), equals('/2021/07/08/daily-jots'));
    });

    test('title', () {
      expect(buildTitle(creationTime), equals('Daily Jots for 08 Jul 2021'));
    });
  });

  group('Test Reference URL Creation', () {
    test('With trailing slash', () {
      expect(buildReferenceUrl('https://myblog.net/', creationTime),
          equals('https://myblog.net/2021/07/08/daily-jots'));
    });

    test('Without trailing slash', () {
      expect(buildReferenceUrl('https://myblog.net', creationTime),
          equals('https://myblog.net/2021/07/08/daily-jots'));
    });

    test('Excess slashes', () {
      expect(buildReferenceUrl('https://myblog.net////', creationTime),
          equals('https://myblog.net/2021/07/08/daily-jots'));
    });
  });

  test('Test Reference Text', () {
    expect(buildReferenceText('http://myblog.net/2021/07/08/dail-jots'),
        equals('(Jotted at: http://myblog.net/2021/07/08/dail-jots)'));
  });
}
